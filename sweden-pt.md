# Mudando para a Suécia

A ideia desse post é reunir algumas informações úteis para alguém que esteja
mudando-se para a Suécia. 

As informações reunidas aqui são baseadas na minha experiência de mudar-se para
a Suécia, mas especificamente Lund na região do Skåne. É a primeira vez que
moro fora do meu país natal, então as informações são de um viajante de
primeira viagem ;)

Esse publicação está em construção, mais informações em breve ;)


## Moradia

Dependendo da região que você vai morar conseguir um lugar para morar pode não
ser a coisa mais fácil. O aluguel de imóveis na Suécia não é igual à outros
lugares (pelo menos não é igual ao Brasil). Existem basicamente 2 tipos de
imóveis para alugar:

* 1a mão (First hand): Imóveis que são alugados por imobiliárias, e para
  conseguir aluga-los você precisa estar em uma fila, onde quem está há mais
  tempo tem prioridade.

* 2a mão (Second hand): Imóveis que são propriedade de um indíviduo e eles está
  alugando, ou imóveis de primeira mão que estão sendo sub-locados.


Claramente existe uma certa falta de imóveis em diversas regiões da Suécia,
como é o caso de Lund e Malmö, portanto conseguir aluguar um desses imóveis
para quem é estrangeiro pode ser difícil. No caso dos de primeira mão, seu
tempo de fila vai contar à partir do momento que entrar na fila (quando tiver
seu Swedish personal idendity number ou só personal number (similar ao CPF), e
se cadastrar nos respectivos sites das imobiliárias. Já nos de 2a mão, muitas
vezes acaba sendo mais fácil pra quem é sueco ficar sabendo por meio de amigos
e familiares.




## Links importantes

* https://www.skatteverket.se/servicelankar/otherlanguages/inenglish/individualsandemployees/movingtosweden.4.7be5268414bea064694c40c.html
